package terminal;

import javax.smartcardio.*;


enum ACR122U_ERRORS {
    NO_ERRORS                   (0x00,  "No error"),
    TIME_OUT                    (0x01,  "Time Out, the target has not answered"),
    CRC_ERROR                   (0x02,  "A CRC error has been detected by the contactless UART"),
    PARITY_ERROR                (0x03,  "A Parity error has been detected by the contactless UART"),
    MIFARE_COLLISION            (0x04,  "During a MIFARE anti-collision/select operation, an erroneous Bit Count has " +
                                        "been detected"),
    MIFARE_FRAMING_ERROR        (0x05,  "Framing error during MIFARE operation"),
    BIT_COLLISION               (0x06,  "An abnormal bit-collision has been detected during bit wise anti-collision " +
                                        "at 106kbps"),
    BUFFER_SIZE_INSUFFICIENT    (0x07,  "Communication buffer size insufficient"),
    BUFFER_OVERFLOW             (0x08,  "RF Buffer overflow has been detected by the contactless UART (bit " +
                                        "BufferOvflof the register CL_ERROR)"),
    RF_FIELD_TIME_OUT           (0x0A,  "In active communication mode, the RF field has not been switched on in time " +
                                        "by the counterpart (as defined in NFCIP-1 standard)"),
    RF_PROTOCOL_ERROR           (0x0B,  "RF Protocol error (cf. reference [4], description of the CL_ERROR register)"),
    TEMPERATURE_ERROR           (0x0D,  "Temperature error: the internal temperature sensor has detected overheating, " +
                                        "and therefore has automatically switched off the antenna drivers"),
    INTERNAL_BUFFER_OVERFLOW    (0x0e,  "Internal buffer overflow"),
    INVALID_PARAMETER           (0x10,  "Invalid parameter (range, format, ...)"),
    DEP_ERROR_1                 (0x12,  "DEP Protocol: The chip configured in target mode does not support the" +
                                        "command received from the initiator (the command received is not one of the" +
                                        "following: ATR_REQ, WUP_REQ, PSL_REQ, DEP_REQ, DSL_REQ, RLS_REQ)"),
    DEP_ERROR_2                 (0x13,  "DEP Protocol / MIFARE / ISO/IEC 14443-4: The data format does not match to" +
                                        "the specification. Depending on the RF protocol used, it can be:\n" +
                                        "• Bad length of RF received frame,\n" +
                                        "• Incorrect value of PCB or PFB,\n" +
                                        "• Invalid or unexpected RF received frame,\n" +
                                        "• NAD or DID incoherence."),
    AUTHENTICATION_ERROR        (0x14,  "MIFARE: Authentication error"),
    UID_BYTE_WRONG              (0x23,  "ISO/IEC 14443-3: UID Check byte is wrong"),
    DEP_ERROR_3                 (0x25,  "DEP Protocol: Invalid device state, the system is in a state which does not " +
                                        "allow the operation"),
    OPERATION_NOT_ALLOWED       (0x26,  "Operation not allowed in this configuration (host controller interface)"),
    BAD_COMMAND                 (0x27,  "This command is not acceptable due to the current context of the chip " +
                                        "(Initiator vs. Target, unknown target number, Target not in the good " +
                                        "state, ...)"),
    CHIP_RELEASED               (0x29,  "The chip configured as target has been released by its initiator"),
    BAD_CARD_ID                 (0x2A,  "ISO/IEC 14443-3B only: the ID of the card does not match, meaning that the " +
                                        "expected card has been exchanged with another one."),
    CARD_DISAPPEARED            (0x2B,  "ISO/IEC 14443-3B only: the card previously activated has disappeared."),
    DEP_ERROR_4                 (0x2C,  "Mismatch between the NFCID3 initiator and the NFCID3 target in DEP " +
                                        "212/424 kbps passive."),
    OVER_CURRENT                (0x2D,  "An over-current event has been detected"),
    DEP_ERROR_5                 (0x2E,  "NAD missing in DEP frame");

    private final int errorCode;
    private final String errorDescription;

    ACR122U_ERRORS(int code, String description) {
        this.errorCode = code;
        this.errorDescription = description;
    }

    public int getErrorCode() { return errorCode; }
    public String getErrorDescription() { return errorDescription; }
}

enum CARD_NAMES {
    MIFARE_CLASSIC_1K           (0x00, 0x01, "Mifare classic 1K card"),
    MIFARE_CLASSIC_4K           (0x00, 0x02, "Mifare classic 2K card"),
    MIFARE_ULTRALIGHT           (0x00, 0x03, "Mifare Ultralight card"),
    MIFARE_MINI                 (0x00, 0x26, "Mifare mini card"),
    TOPAZ_JEWEL                 (0xF0, 0x04, "Topaz or Jewel card"),
    FELICA_212K                 (0xF0, 0x11, "FeliCA 212K card"),
    FELICA_424K                 (0xF0, 0x12, "FeliCA 424K card"),
    UNDEFINED                   (0xFF, "Undefined");

    private final byte[] cardCode;
    private final String cardName;

    CARD_NAMES(int firstCode, int secondCode, String name) {
        cardCode = new byte[2];
        cardCode[0] = (byte)firstCode;
        cardCode[1] = (byte)secondCode;
        cardName = name;
    }

    CARD_NAMES(int code, String name) {
        cardCode = new byte[1];
        cardCode[0] = (byte)code;
        cardName = name;
    }

    public byte[] getCardCode() { return cardCode; }
    public String getCardName() { return cardName; }

    public static CARD_NAMES fromBytes(int firstCode, int secondCode) {
        switch (firstCode) {
            case 0x00:
                switch (secondCode) {
                    case 0x01:
                        return CARD_NAMES.MIFARE_CLASSIC_1K;
                    case 0x02:
                        return CARD_NAMES.MIFARE_CLASSIC_4K;
                    case 0x03:
                        return CARD_NAMES.MIFARE_ULTRALIGHT;
                    case 0x26:
                        return CARD_NAMES.MIFARE_MINI;
                    default:
                        return CARD_NAMES.UNDEFINED;
                }
            case 0xF0:
                switch (secondCode) {
                    case 0x04:
                        return CARD_NAMES.TOPAZ_JEWEL;
                    case 0x11:
                        return CARD_NAMES.FELICA_212K;
                    case 0x12:
                        return CARD_NAMES.FELICA_424K;
                    default:
                        return CARD_NAMES.UNDEFINED;
                }
            default:
                return CARD_NAMES.UNDEFINED;
        }
    }

    public static CARD_NAMES fromBytes(byte firstCode, byte secondCode) {
        return fromBytes((int)firstCode, (int)secondCode);
    }
}

enum CARD_STANDARDS {
    ISO_14443A_P3   (0x03, "ISO 14443A, Part 3"),
    UNKNOWN         (0xFF, "Unknown standard");

    private final byte standardCode;
    private final String standardDescription;

    CARD_STANDARDS (int code, String desc) {
        standardCode = (byte)code;
        standardDescription = desc;
    }

    public String getStandardDescription() { return standardDescription; }
    public byte getStandardCode() { return standardCode; }
    public int getSectorCount() {
        //TODO: write this function
        return 16;
    }

    public static CARD_STANDARDS fromByte(byte code) {
        switch (code) {
            case 0x03:
                return CARD_STANDARDS.ISO_14443A_P3;
            default:
                return CARD_STANDARDS.UNKNOWN;
        }
    }
}


public class ACR122U {
    private Card card;

    public ACR122U(Card card) {
        this.card = card;
    }

    public String getFirmwareVersion() {
        try {
            CardChannel cardChannel = card.getBasicChannel();
            byte[] data = {0x00, 0x00};
            CommandAPDU command = new CommandAPDU(0xFF, 0x00, 0x48, 0x00, data);
            ResponseAPDU answer = cardChannel.transmit(command);
            return new String(answer.getBytes());
        } catch (CardException e) {
            return "";
        }
    }

    public String getCardName(ATR atr) {
        byte[] atrBytes = atr.getBytes();
        return CARD_NAMES.fromBytes(atrBytes[13], atrBytes[14]).getCardName();
    }

    public String getCardStandard(ATR atr) {
        return CARD_STANDARDS.fromByte(atr.getBytes()[12]).getStandardDescription();
    }

    public byte[] getSectorData(int sectorNumber, int blockNumber) {
        try {
            int block = sectorNumber*4 + blockNumber;
            CardChannel channel = card.getBasicChannel();
            CommandAPDU command = new CommandAPDU(0xFF, 0xB0, 0x00, block, 16);
            ResponseAPDU response = channel.transmit(command);
            return response.getData();
        } catch (CardException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeSectorData(int sectorNumber, int blockNumber, byte[] data) {
        try {
            int block = sectorNumber*4 + blockNumber;
            CardChannel channel = card.getBasicChannel();
            CommandAPDU command = new CommandAPDU(0xFF, 0xD6, 0x00, (byte)block, data);
            ResponseAPDU response = channel.transmit(command);
        } catch (CardException e) {
            e.printStackTrace();
        }
    }

    public boolean authenticateKey(byte[] key, int sector, int block, boolean type1) {
        byte keyNumber = 0x00;
        if (type1) keyNumber = 0x01;
        try {
            CardChannel channel = card.getBasicChannel();
            CommandAPDU command = new CommandAPDU(0xFF, 0x082, 0x00, keyNumber, key, 0x00, 0x06);
            ResponseAPDU response = channel.transmit(command);
            if (response.getSW1()==0x63) {
                System.out.println("Problem with loading key");
                return false;
            }

            byte[] data = {0x01, 0x00, (byte)(sector*4 + block), 0x60, keyNumber};
            command = new CommandAPDU(0xFF, 0x86, 0x00, 0x00, data, 0x00, 0x05);
            response = channel.transmit(command);
            if (response.getSW1()==0x63) {
                System.out.println("Problem with loading key");
                return false;
            }
        } catch (CardException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    public static String bytesToHex(byte b) {
        byte[] arg = new byte[1];
        arg[0] = b;
        return bytesToHex(arg);
    }
    public static String bytesToHex(int i) { return bytesToHex((byte)i); }

    public static byte[] stringToHex(String str) {
        if (str.length() % 2 == 1) str = "0" + str;
        char[] charArray = str.toLowerCase().toCharArray();
        byte[] ret = new byte[str.length()/2];

        String buff = new String();
        for (int i=0; i<charArray.length; i++) {
            buff += charArray[i];
            if (buff.length() == 2) {
                ret[i/2] = (byte)(Integer.parseInt(buff, 16) & 0xFF);
                buff = "";
            }
        }
        return ret;
    }
}
