package terminal;

public enum ConditionName {
    READ,
    WRITE,
    INCREMENT,
    DECREMENT
}