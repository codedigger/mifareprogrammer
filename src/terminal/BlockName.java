package terminal;

/**
 * Enum BlockNames
 *
 * Defines data blocks names
 */
public enum BlockName {
    BLOCK_1,
    BLOCK_2,
    BLOCK_3
}
