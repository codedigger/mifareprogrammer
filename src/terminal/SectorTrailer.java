package terminal;

/**
 * Class Bit
 *
 * This class represents one information about access condition.
 * Key can (or can't) do something.
 *
 * It is wrapper to boolean value.
 */
class Bit {
    private boolean val;

    public Bit(boolean val) { this.val = val; }
    public Bit() { }

    public void set() { val = true; }
    public void clear() { val = false; }
    public void setVal(boolean val) { this.val = val; }
    public boolean getVal() { return val; }
}

/**
 * Class BlockAccessCondition
 *
 * Class represents four access conditions to data block: key can (or can't) read, write, increment or
 * decrement data written in data block.
 */
class BlockAccessCondition {
    private Bit canRead;
    private Bit canWrite;
    private Bit canIncrement;
    private Bit canDecrement;

    /**
     * Default class creator.
     */
    public BlockAccessCondition() {
        canRead = new Bit();
        canWrite = new Bit();
        canIncrement = new Bit();
        canDecrement = new Bit();
    }

    /**
     * Class creator with default boolean values
     * @param canRead       key can read from block
     * @param canWrite      key can write to block
     * @param canIncrement  key can increment data in block
     * @param canDecrement  key can decrement (and other op.) data in block
     */
    public BlockAccessCondition(boolean canRead, boolean canWrite, boolean canIncrement, boolean canDecrement) {
        this.canRead = new Bit();
        this.canWrite = new Bit();
        this.canIncrement = new Bit();
        this.canDecrement = new Bit();
        setCanRead(canRead);
        setCanWrite(canWrite);
        setCanIncrement(canIncrement);
        setCanDecrement(canDecrement);
    }

    /**
     * Class creator with default Bit values
     * @param canRead       key can read from block
     * @param canWrite      key can write to block
     * @param canIncrement  key can increment data in block
     * @param canDecrement  key can decrement (and other op.) data in block
     */
    public BlockAccessCondition(Bit canRead, Bit canWrite, Bit canIncrement, Bit canDecrement) {
        setCanRead(canRead);
        setCanWrite(canWrite);
        setCanIncrement(canIncrement);
        setCanDecrement(canDecrement);
    }

    /*
    All class setters
     */
    public void setCanRead(Bit canRead) { this.canRead = canRead; }
    public void setCanRead(boolean canRead) { this.canRead.setVal(canRead); }

    public void setCanWrite(Bit canWrite) { this.canWrite = canWrite; }
    public void setCanWrite(boolean canWrite) { this.canWrite.setVal(canWrite); }

    public void setCanIncrement(Bit canIncrement) { this.canIncrement = canIncrement; }
    public void setCanIncrement(boolean canIncrement) { this.canIncrement.setVal(canIncrement); }

    public void setCanDecrement(Bit canDecrement) { this.canDecrement = canDecrement; }
    public void setCanDecrement(boolean canDecrement) { this.canDecrement.setVal(canDecrement); }

    /**
     * Setting conditions according to C values when kay "A" is used
     * @param c1        C1 value
     * @param c2        C2 value
     * @param c3        C3 value
     */
    public void setFromCValues_A(boolean c1, boolean c2, boolean c3) {
        if (!c1 && !c2 && !c3) {
            setCanWrite(true);
            setCanRead(true);
            setCanIncrement(true);
            setCanDecrement(true);
        } else if (!c1 && c2 && !c3) {
            setCanWrite(true);
            setCanRead(false);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && !c2 && !c3) {
            setCanWrite(false);
            setCanRead(true);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && c2 && !c3) {
            setCanWrite(false);
            setCanRead(true);
            setCanIncrement(false);
            setCanDecrement(true);
        } else if (!c1 && !c2 && c3) {
            setCanWrite(false);
            setCanRead(true);
            setCanIncrement(false);
            setCanDecrement(true);
        } else if (!c1 && c2 && c3) {
            setCanWrite(false);
            setCanRead(false);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && !c2 && c3) {
            setCanWrite(false);
            setCanRead(false);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && c2 && c3) {
            setCanWrite(false);
            setCanRead(false);
            setCanIncrement(false);
            setCanDecrement(false);
        }
    }

    /**
     * Setting conditions according to C values when kay "B" is used
     * @param c1        C1 value
     * @param c2        C2 value
     * @param c3        C3 value
     */
    public void setFromCValues_B(boolean c1, boolean c2, boolean c3) {
        if (!c1 && !c2 && !c3) {
            setCanWrite(true);
            setCanRead(true);
            setCanIncrement(true);
            setCanDecrement(true);
        } else if (!c1 && c2 && !c3) {
            setCanWrite(true);
            setCanRead(false);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && !c2 && !c3) {
            setCanWrite(true);
            setCanRead(true);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && c2 && !c3) {
            setCanWrite(true);
            setCanRead(true);
            setCanIncrement(true);
            setCanDecrement(true);
        } else if (!c1 && !c2 && c3) {
            setCanWrite(false);
            setCanRead(true);
            setCanIncrement(false);
            setCanDecrement(true);
        } else if (!c1 && c2 && c3) {
            setCanWrite(true);
            setCanRead(true);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && !c2 && c3) {
            setCanWrite(false);
            setCanRead(true);
            setCanIncrement(false);
            setCanDecrement(false);
        } else if (c1 && c2 && c3) {
            setCanWrite(false);
            setCanRead(false);
            setCanIncrement(false);
            setCanDecrement(false);
        }
    }

    /*
    All class getters
     */
    public Bit getCanRead() { return canRead; }
    public Bit getCanWrite() { return canWrite; }
    public Bit getCanIncrement() { return canIncrement; }
    public Bit getCanDecrement() { return canDecrement; }
}

class KeyAccessCondition {
    private Bit isReadable;
    private Bit isWritable;
    private BlockAccessCondition block1Conditions;
    private BlockAccessCondition block2Conditions;
    private BlockAccessCondition block3Conditions;
    private KeyType keyType;

    public KeyAccessCondition() {
        isReadable = new Bit(true);
        isWritable = new Bit(true);

        block1Conditions = new BlockAccessCondition(true, true, true, true);
        block2Conditions = new BlockAccessCondition(true, true, true, true);
        block3Conditions = new BlockAccessCondition(true, true, true, true);

        keyType = KeyType.KEY_A;
    }

    public void setIsReadable(Bit isReadable) { this.isReadable = isReadable; }
    public void setIsReadable(boolean isReadable) { this.isReadable.setVal(isReadable); }

    public void setIsWritable(Bit isWritable) { this.isWritable = isWritable; }
    public void setIsWritable(boolean isWritable) { this.isWritable.setVal(isWritable); }

    public void setReadCondition(BlockName block, boolean condition) {
        switch (block) {
            case BLOCK_1:
                block1Conditions.setCanRead(condition);
                break;
            case BLOCK_2:
                block2Conditions.setCanRead(condition);
                break;
            case BLOCK_3:
                block3Conditions.setCanRead(condition);
                break;
        }
    }

    public void setWriteCondition(BlockName block, boolean condition) {
        switch (block) {
            case BLOCK_1:
                block1Conditions.setCanWrite(condition);
                break;
            case BLOCK_2:
                block2Conditions.setCanWrite(condition);
                break;
            case BLOCK_3:
                block3Conditions.setCanWrite(condition);
                break;
        }
    }

    public void setIncrementCondition(BlockName block, boolean condition) {
        switch (block) {
            case BLOCK_1:
                block1Conditions.setCanIncrement(condition);
                break;
            case BLOCK_2:
                block2Conditions.setCanIncrement(condition);
                break;
            case BLOCK_3:
                block3Conditions.setCanIncrement(condition);
                break;
        }
    }

    public void setDecrementCondition(BlockName block, boolean condition) {
        switch (block) {
            case BLOCK_1:
                block1Conditions.setCanDecrement(condition);
                break;
            case BLOCK_2:
                block2Conditions.setCanDecrement(condition);
                break;
            case BLOCK_3:
                block3Conditions.setCanDecrement(condition);
                break;
        }
    }

    public void setBlockConditionsByC(BlockName block, boolean c1, boolean c2, boolean c3) {
        switch (block) {
            case BLOCK_1:
                if (keyType == KeyType.KEY_A) block1Conditions.setFromCValues_A(c1, c2, c3);
                else block1Conditions.setFromCValues_B(c1, c2, c3);
                break;
            case BLOCK_2:
                if (keyType == KeyType.KEY_A) block2Conditions.setFromCValues_A(c1, c2, c3);
                else block2Conditions.setFromCValues_B(c1, c2, c3);
                break;
            case BLOCK_3:
                if (keyType == KeyType.KEY_A) block3Conditions.setFromCValues_A(c1, c2, c3);
                else block3Conditions.setFromCValues_B(c1, c2, c3);
        }

    }

    public void setKeyType(KeyType keyType) { this.keyType = keyType; }

    public Bit getIsReadable() { return isReadable; }
    public Bit getIsWritable() { return isWritable; }
    public KeyType getKeyType() { return keyType; }

    public boolean getCondition(BlockName block, ConditionName condition) {
        BlockAccessCondition selectedBlock;

        // which block?
        switch (block) {
            case BLOCK_1:
                selectedBlock = block1Conditions;
                break;
            case BLOCK_2:
                selectedBlock = block2Conditions;
                break;
            default:
                selectedBlock = block3Conditions;
                break;
        }

        switch (condition) {
            case READ: return selectedBlock.getCanRead().getVal();
            case WRITE: return selectedBlock.getCanWrite().getVal();
            case INCREMENT: return selectedBlock.getCanIncrement().getVal();
            case DECREMENT: return selectedBlock.getCanDecrement().getVal();
        }
        return false;
    }
}

/**
 * Class SectorTrailer
 *
 * Class represents all access conditions written in sector trailer
 */
public class SectorTrailer {
    private KeyAccessCondition keyA;
    private KeyAccessCondition keyB;

    public SectorTrailer() {
        keyA = new KeyAccessCondition();
        keyB = new KeyAccessCondition();
        keyB.setKeyType(KeyType.KEY_B);
    }

    public SectorTrailer(byte[] trailer) {
        byte byte1 = trailer[0];
        byte byte2 = trailer[1];
        byte byte3 = trailer[2];

        Bit negC23 = new Bit();
        Bit negC22 = new Bit();
        Bit negC21 = new Bit();
        Bit negC20 = new Bit();
        Bit negC13 = new Bit();
        Bit negC12 = new Bit();
        Bit negC11 = new Bit();
        Bit negC10 = new Bit();
        Bit[] byte1Array = {negC23, negC22, negC21, negC20, negC13, negC12, negC11, negC10};

        Bit c13 = new Bit();
        Bit c12 = new Bit();
        Bit c11 = new Bit();
        Bit c10 = new Bit();
        Bit negC33 = new Bit();
        Bit negC32 = new Bit();
        Bit negC31 = new Bit();
        Bit negC30 = new Bit();
        Bit[] byte2Array = {c13, c12, c11, c10, negC33, negC32, negC31, negC30 };

        Bit c33 = new Bit();
        Bit c32 = new Bit();
        Bit c31 = new Bit();
        Bit c30 = new Bit();
        Bit c23 = new Bit();
        Bit c22 = new Bit();
        Bit c21 = new Bit();
        Bit c20 = new Bit();
        Bit[] byte3Array = {c33, c32, c31, c30, c23, c22, c21, c20 };

        int mask=0b10000000;
        for (int i=0; i<8; i++) {
            System.out.println("Mask: "+mask);
            if ((byte1 & mask) != 0) byte1Array[i].set();
            else byte1Array[i].clear();

            if ((byte2 & mask) != 0) byte2Array[i].set();
            else byte2Array[i].clear();

            if ((byte3 & mask) != 0) byte3Array[i].set();
            else byte3Array[i].clear();

            mask = mask >> 1;
        }

        System.out.println("\nbyte1Array: "+byte1);
        for (Bit bit : byte1Array) {
            System.out.print(bit.getVal()+", ");
        }
        System.out.println("\nbyte2Array: "+byte2);
        for (Bit bit : byte2Array) {
            System.out.print(bit.getVal()+", ");
        }
        System.out.println("\nbyte3Array: "+byte3);
        for (Bit bit : byte3Array) {
            System.out.print(bit.getVal()+", ");
        }

        if (c10.getVal() == negC10.getVal()) throw new VerifyError();
        if (c11.getVal() == negC11.getVal()) throw new VerifyError();
        if (c12.getVal() == negC12.getVal()) throw new VerifyError();
        if (c13.getVal() == negC13.getVal()) throw new VerifyError();
        if (c20.getVal() == negC20.getVal()) throw new VerifyError();
        if (c21.getVal() == negC21.getVal()) throw new VerifyError();
        if (c22.getVal() == negC22.getVal()) throw new VerifyError();
        if (c23.getVal() == negC23.getVal()) throw new VerifyError();
        if (c30.getVal() == negC30.getVal()) throw new VerifyError();
        if (c31.getVal() == negC31.getVal()) throw new VerifyError();
        if (c32.getVal() == negC32.getVal()) throw new VerifyError();
        if (c33.getVal() == negC33.getVal()) throw new VerifyError();

        keyA = new KeyAccessCondition();
        keyB = new KeyAccessCondition();
        keyB.setKeyType(KeyType.KEY_B);

        keyA.setBlockConditionsByC(BlockName.BLOCK_1, c10.getVal(), c20.getVal(), c30.getVal());
        keyB.setBlockConditionsByC(BlockName.BLOCK_1, c10.getVal(), c20.getVal(), c30.getVal());

        keyA.setBlockConditionsByC(BlockName.BLOCK_2, c11.getVal(), c21.getVal(), c31.getVal());
        keyB.setBlockConditionsByC(BlockName.BLOCK_2, c11.getVal(), c21.getVal(), c31.getVal());

        keyA.setBlockConditionsByC(BlockName.BLOCK_3, c12.getVal(), c22.getVal(), c32.getVal());
        keyB.setBlockConditionsByC(BlockName.BLOCK_3, c12.getVal(), c22.getVal(), c32.getVal());
    }

    public boolean getBlockCondition(BlockName block, KeyType key, ConditionName condition) {
        if (key == KeyType.KEY_A) return keyA.getCondition(block, condition);
        else return keyB.getCondition(block, condition);
    }

}
