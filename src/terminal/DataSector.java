package terminal;

public class DataSector {
    byte[] block1;
    byte[] block2;
    byte[] block3;
    byte[] block4;
    String name;

    public DataSector(String name) {
        block1 = null;
        block2 = null;
        block3 = null;
        block4 = null;
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public byte[] getBlock1() { return block1; }
    public byte[] getBlock2() { return block2; }
    public byte[] getBlock3() { return block3; }
    public byte[] getBlock4() { return block4; }

    public void setBlock1(byte[] block1) {
        this.block1 = block1;
    }

    public void setBlock2(byte[] block2) {
        this.block2 = block2;
    }

    public void setBlock3(byte[] block3) {
        this.block3 = block3;
    }

    public void setBlock4(byte[] block4) {
        this.block4 = block4;
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
}
