package gui;

import javax.smartcardio.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.xml.crypto.Data;

import com.sun.istack.internal.ByteArrayDataSource;
import terminal.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;


class HexFilter extends DocumentFilter {
    private int limit;
    public HexFilter(int limit) {
        this.limit = limit;
    }

    private boolean test(String text) {
        String hexChars = "1234567890ABCDEF";
        for (char c : text.toCharArray()) {
            if (hexChars.indexOf(c) == -1) return false;
        }
        return true;
    }

    @Override
    public void insertString(FilterBypass fb, int offset, String string, AttributeSet attr) throws BadLocationException {
        Document doc = fb.getDocument();
        if (doc.getLength() >= limit) return;

        string = string.toUpperCase();
        if (test(string)) super.insertString(fb, offset, string, attr);
    }

    @Override
    public void replace(FilterBypass fb, int offset, int length, String text,
                        AttributeSet attrs) throws BadLocationException {
        Document doc = fb.getDocument();
        if (doc.getLength() >= limit) return;

        text = text.toUpperCase();
        if (test(text)) super.replace(fb, offset, length, text, attrs);

    }
}

public class MainWindow implements TerminalListener, CardListener {
    private JPanel mainPanel;
    private JLabel terminalName;
    private JLabel cardName;
    private JLabel firmwareVersion;
    private JLabel cardStandard;
    private JTextField key2;
    private JTextField block1;
    private JTextField block4;
    private JTextField block3;
    private JTextField block2;
    private JRadioButton autARadioButton;
    private JRadioButton autBRadioButton;
    private JButton authenticateKey1;
    private JButton authenticateKey2;
    private JList sectorList;
    private JButton readSector;
    private JButton writeSector;
    private JTextField key1;
    private JTextField keyAVal;
    private JTextField keyBVal;
    private JCheckBox block2KeyBDecrement;
    private JCheckBox block2KeyBIncrement;
    private JCheckBox block2KeyBWrite;
    private JCheckBox block2KeyBRead;
    private JCheckBox block2KeyADecrement;
    private JCheckBox block2KeyAIncrement;
    private JCheckBox block2KeyARead;
    private JCheckBox block2KeyAWrite;
    private JCheckBox block3KeyADecrement;
    private JCheckBox block3KeyAIncrement;
    private JCheckBox block3KeyARead;
    private JCheckBox block3KeyAWrite;
    private JCheckBox block3KeyBDecrement;
    private JCheckBox block3KeyBIncrement;
    private JCheckBox block3KeyBRead;
    private JCheckBox block3KeyBWrite;
    private JButton block1OptionsButton;
    private JButton block2OptionsButton;
    private JButton block3OptionsButton;
    private JCheckBox block1KeyBWrite;
    private JCheckBox block1KeyBRead;
    private JCheckBox block1KeyBIncrement;
    private JCheckBox block1KeyBDecrement;
    private JCheckBox block1KeyAWrite;
    private JCheckBox block1KeyARead;
    private JCheckBox block1KeyAIncrement;
    private JCheckBox block1KeyADecrement;
    private JPanel block1OptionsPanel;
    private JPanel block2OptionsPanel;
    private JPanel block3OptionsPanel;

    private JFrame mainFrame = new JFrame("Main window");

    DefaultListModel<DataSector> sectorListModel;
    ACR122U acrCard;

    public MainWindow() {
        block1OptionsPanel.setVisible(false);
        block2OptionsPanel.setVisible(false);
        block3OptionsPanel.setVisible(false);

        PlainDocument doc = (PlainDocument)key1.getDocument();
        doc.setDocumentFilter(new HexFilter(12));

        doc = (PlainDocument)key2.getDocument();
        doc.setDocumentFilter(new HexFilter(12));

//        doc = (PlainDocument)block1.getDocument();
//        doc.setDocumentFilter(new HexFilter(32));
//
//        doc = (PlainDocument)block2.getDocument();
//        doc.setDocumentFilter(new HexFilter(32));
//
//        doc = (PlainDocument)block3.getDocument();
//        doc.setDocumentFilter(new HexFilter(32));
//
//        doc = (PlainDocument)block4.getDocument();
//        doc.setDocumentFilter(new HexFilter(32));

        sectorList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                DataSector data = (DataSector)sectorList.getSelectedValue();

                if (data.getBlock1() == null) {
                    block1.setEnabled(false);
                    block1.setText("");
                } else {
                    block1.setEnabled(true);
                    block1.setText(DataSector.bytesToHex(data.getBlock1()));
                }

                if (data.getBlock2() == null) {
                    block2.setEnabled(false);
                    block2.setText("");
                } else {
                    block2.setEnabled(true);
                    block2.setText(DataSector.bytesToHex(data.getBlock2()));
                }

                if (data.getBlock3() == null) {
                    block3.setEnabled(false);
                    block3.setText("");
                } else {
                    block3.setEnabled(true);
                    block3.setText(DataSector.bytesToHex(data.getBlock3()));
                }

                if (data.getBlock4() == null) {
                    block4.setEnabled(false);
                    block4.setText("");
                } else {
                    block4.setEnabled(true);
                    byte[] block4Data = data.getBlock4();
                    block4.setText(DataSector.bytesToHex(block4Data));
                    assignBlockData(block4Data);
                }
            }
        });

        authenticateKey1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int sector = sectorList.getSelectedIndex();
                byte[] key = ACR122U.stringToHex(key1.getText());
                if (acrCard.authenticateKey(key, sector, 0, false)) System.out.println("Key A loaded");
                super.mouseClicked(e);
            }
        });

        authenticateKey2.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                byte[] key = ACR122U.stringToHex(key2.getText());
                int sector = sectorList.getSelectedIndex();
                if (acrCard.authenticateKey(key, sector, 0, true)) System.out.println("Key B loaded");
                super.mouseClicked(e);
            }
        });

        readSector.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                DataSector data = (DataSector)sectorList.getSelectedValue();
                int selectedSector = sectorList.getSelectedIndex();
                if (selectedSector == -1) selectedSector=0;
                byte[] data1 = acrCard.getSectorData(selectedSector, 0);
                byte[] data2 = acrCard.getSectorData(selectedSector, 1);
                byte[] data3 = acrCard.getSectorData(selectedSector, 2);
                byte[] data4 = acrCard.getSectorData(selectedSector, 3);

                data.setBlock1(data1);
                data.setBlock2(data2);
                data.setBlock3(data3);
                data.setBlock4(data4);

                block1.setEnabled(true);
                block1.setText(DataSector.bytesToHex(data1));

                block2.setEnabled(true);
                block2.setText(DataSector.bytesToHex(data2));

                block3.setEnabled(true);
                block3.setText(DataSector.bytesToHex(data3));

                block4.setEnabled(true);
                block4.setText(DataSector.bytesToHex(data4));
                assignBlockData(data4);

                super.mouseClicked(e);
            }
        });

        block1OptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (block1OptionsPanel.isVisible()) {
                    block1OptionsPanel.setVisible(false);
                    optionsPanelHided();
                }
                else {
                    block1OptionsPanel.setVisible(true);
                    optionsPanelShowed();
                }
            }
        });

        block2OptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (block2OptionsPanel.isVisible()) {
                    block2OptionsPanel.setVisible(false);
                    optionsPanelHided();
                }
                else {
                    block2OptionsPanel.setVisible(true);
                    optionsPanelShowed();
                }
            }
        });

        block3OptionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (block3OptionsPanel.isVisible()) {
                    block3OptionsPanel.setVisible(false);
                    optionsPanelHided();
                }
                else {
                    block3OptionsPanel.setVisible(true);
                    optionsPanelShowed();
                }
            }
        });

        writeSector.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                byte[] data = {(byte)0x10, (byte)0x11, (byte)0x12, (byte)0x13, (byte)0x14, (byte)0x15,
                               (byte)0xFF, 0x07, (byte)0x80, 0x69,
                               0x10, 0x11, 0x12, 0x13, 0x14, 0x15};
                acrCard.writeSectorData(0, 3, data);
            }
        });
    }

    private void optionsPanelShowed() {
        Dimension size = mainPanel.getPreferredSize();
        size.height += 60;
        mainPanel.setPreferredSize(size);
        mainFrame.pack();
    }

    private void optionsPanelHided() {
        Dimension size = mainPanel.getPreferredSize();
        size.height -= 60;
        mainPanel.setPreferredSize(size);
        mainFrame.pack();
    }

    private void assignBlockData(byte[] block) {
        byte[] keyA = new byte[6];
        byte[] keyB = new byte[6];
        byte[] conditions = new byte[4];

        System.arraycopy(block, 0, keyA, 0, 6);
        System.arraycopy(block, 6, conditions, 0, 4);
        System.arraycopy(block, 10, keyB, 0, 6);

        keyAVal.setText(DataSector.bytesToHex(keyA));
        keyBVal.setText(DataSector.bytesToHex(keyB));
        SectorTrailer trailer = new SectorTrailer(conditions);

        // Setting block 1, key A
        block1KeyARead.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_A,
                ConditionName.READ
        ));
        block1KeyAWrite.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_A,
                ConditionName.WRITE
        ));
        block1KeyAIncrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_A,
                ConditionName.INCREMENT
        ));
        block1KeyADecrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_A,
                ConditionName.DECREMENT
        ));

        // Setting block 1, key B
        block1KeyBRead.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_B,
                ConditionName.READ
        ));
        block1KeyBWrite.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_B,
                ConditionName.WRITE
        ));
        block1KeyBIncrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_B,
                ConditionName.INCREMENT
        ));
        block1KeyBDecrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_1,
                KeyType.KEY_B,
                ConditionName.DECREMENT
        ));

        // Setting block 2, key A
        block2KeyARead.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_A,
                ConditionName.READ
        ));
        block2KeyAWrite.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_A,
                ConditionName.WRITE
        ));
        block2KeyAIncrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_A,
                ConditionName.INCREMENT
        ));
        block2KeyADecrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_A,
                ConditionName.DECREMENT
        ));

        // Setting block 2, key B
        block2KeyBRead.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_B,
                ConditionName.READ
        ));
        block2KeyBWrite.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_B,
                ConditionName.WRITE
        ));
        block2KeyBIncrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_B,
                ConditionName.INCREMENT
        ));
        block2KeyBDecrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_2,
                KeyType.KEY_B,
                ConditionName.DECREMENT
        ));

        // Setting block 3, key A
        block3KeyARead.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_A,
                ConditionName.READ
        ));
        block3KeyAWrite.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_A,
                ConditionName.WRITE
        ));
        block3KeyAIncrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_A,
                ConditionName.INCREMENT
        ));
        block3KeyADecrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_A,
                ConditionName.DECREMENT
        ));

        // Setting block 3, key B
        block3KeyBRead.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_B,
                ConditionName.READ
        ));
        block3KeyBWrite.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_B,
                ConditionName.WRITE
        ));
        block3KeyBIncrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_B,
                ConditionName.INCREMENT
        ));
        block3KeyBDecrement.setSelected(trailer.getBlockCondition(
                BlockName.BLOCK_3,
                KeyType.KEY_B,
                ConditionName.DECREMENT
        ));
    }

    public static MainWindow main() {
        MainWindow mainWindow = new MainWindow();
        mainWindow.mainFrame.setContentPane(mainWindow.mainPanel);
        mainWindow.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainWindow.mainFrame.pack();
        mainWindow.mainFrame.setVisible(true);

        return mainWindow;
    }

    @Override
    public void onTerminalAppears(TerminalThread sender, CardTerminal terminal) {
        terminalName.setText(terminal.getName());
        CardThread cardThread = sender.getCardThread();
        cardThread.addListener(CardAction.onCardConnected, this);
        cardThread.addListener(CardAction.onCardDisconnected, this);
    }

    @Override
    public void onTerminalDisappears() {
        terminalName.setText("Brak czytnika");
    }

    @Override
    public void onCardConnected(CardThread sender, Card card) {
        acrCard = new ACR122U(card);
        firmwareVersion.setText(acrCard.getFirmwareVersion());
        cardName.setText(acrCard.getCardName(card.getATR()));
        cardStandard.setText(acrCard.getCardStandard(card.getATR()));

        for (int i=0; i<16; i++) {
            sectorListModel.addElement(new DataSector("Sector "+i));
        }
    }

    @Override
    public void onCardDisconnected() {
        cardName.setText("Brak karty");
        cardStandard.setText("");
        sectorListModel.clear();
    }

    private void createUIComponents() {
        sectorListModel = new DefaultListModel<>();
        sectorList = new JList(sectorListModel);

        key1 = new JTextField(12);
        key2 = new JTextField(12);
        block1 = new JTextField(32);
        block2 = new JTextField(32);
        block3 = new JTextField(32);
        block4 = new JTextField(32);
    }
}
